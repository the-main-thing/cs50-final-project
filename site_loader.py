from recorder import Record

def load_site(url=None, user_id=None, pages_to_load=0):
    next_link = None
    for page in range(pages_to_load):
        print("load_site, link #", page)
        if next_link:
            url = next_link
        record = Record(url, user_id)
        if not record:
            return None
        record_succeed = record.write_page()
        if not record_succeed:
            print("load_site error: record_succeed is not True")
            return None
        next_link = record.next_url
        if not next_link:
            print("load_site warning: next_link is None")
            return None
    print("load {0} complete!".format(record.domain_name))
    return True
