import os
#import time
from urllib.parse import urlparse, urljoin

from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from url_normalize import url_normalize
from pyvirtualdisplay import Display

from settings import WEBDRIVER_PATH, DEBUG_STATUS

def resolve_link(current_page, link):
    if not current_page or not link:
        return None
    if link[:1] != current_page[:1]:
        link = urljoin(current_page, link)
    return url_normalize(link)

def domain(link, skip_url_check=False):
    """ Extract domain name from given link. For example:
        "http://www.example.com/example/page3" will turn into "example.com"
    """
    # Cheat for skiping unnessesary url normalization
    if not skip_url_check:
        link = url_normalize(link)
    if not link:
        return None

    link = urlparse(link)
    domain = str(link.netloc)
    if len(domain) > 4 and domain[:4] == "www.":
        domain = domain[4:]
    return domain

def render(src, skip_url_check=False):
    """ Render web page using selenium and PhantomJS headless browser """
    # Cheat for skiping unnessesary url normalization
    if not skip_url_check:
        src = url_normalize(src)
    if not src:
        return None

    chrome = WEBDRIVER_PATH
    #os.environ["webdriver.chrome.driver"] = chrome
    options = Options()
    options.add_argument("start-maximized")
    options.add_argument("disable-infobars")
    options.add_argument("--disable-extensions")
    display = Display(visible=0, size=[1600, 1200])
    display.start()
    #browser = webdriver.Chrome(chrome_options=options)
    browser = webdriver.Chrome(executable_path=chrome, chrome_options=options)
    if DEBUG_STATUS:
        print("selenium: browser created")
    browser.get(src)
    if DEBUG_STATUS:
        print("selenium: get src")
    try:
        #loaded = WebDriverWait(browser, 20).until(EC.text_to_be_present_in_element((By.TAG_NAME, "body"), " "))
        if (DEBUG_STATUS):
            #print("text is loaded")
            print("selenium: wait for body")
        body = WebDriverWait(browser, 20).until(EC.visibility_of_element_located((By.TAG_NAME, "body")))
        if (DEBUG_STATUS):
            print("selenium: waiting is over")
            browser.save_screenshot('page.png')
        html = body.get_attribute('innerHTML')
    except Exception:
        if DEBUG_STATUS:
            print("selenium: exception")
        browser.quit()
    finally:
        browser.quit()
    if DEBUG_STATUS:
        print("selenium: browser quit")
    display.stop()
    return html