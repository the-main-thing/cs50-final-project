from sqlalchemy.exc import IntegrityError
from sqlalchemy import func

from pages import Page
from url_normalize import url_normalize
from model import db, Domain, Link, Text, User
from settings import DEBUG_STATUS

def add_domain_to_db(domain_name, user):
    if not domain_name or not user:
        print("add_domain_to_db error: domain or user is None")
        return None
    try:
        domain = Domain(domain_name)
        db.session.add(domain)
        db.session.commit()
    except IntegrityError:
        db.session.rollback()
        domain = Domain.query.filter_by(domain=domain_name).first()
    domain.users.append(user)
    db.session.commit()
    return domain

def add_link_to_db(url, domain, just_add=False):
    if not url or not domain:
        print("add_link_to_db error: url or domain is None")
        return None
    try:
        link = Link(url, domain)
        db.session.add(link)
        db.session.commit()
        if DEBUG_STATUS:
            print("RECORDER add_link: link added")
    except IntegrityError:
        db.session.rollback()
        if just_add:
            return True
        link = Link.query.filter_by(link=url).first()
        if DEBUG_STATUS:
            print("RECORDER add_link: link queried")
    if just_add:
        return True
    link.visits_count = link.visits_count + 1
    db.session.commit()
    return link

def add_text_to_db(text, link, domain):
    if not text or not link:
        print("add_text_to_db error: text or link is None")
        return None
    try:
        text_row = Text(text, link, domain)
        db.session.add(text_row)
        db.session.commit()
        if DEBUG_STATUS:
            print("Recorder add_text_to_db: text actually added")
    except IntegrityError:
        db.session.rollback()
        if DEBUG_STATUS:
            print("Recorder add_text_to_db: text DO NOT added")
        return True
    return text_row


class Record():
    def __init__(self, url=None, user=None):
        try:
            url = str(url)
            user = int(user)
            if not url or not user:
                print("Record creation error: url or user is None")
                return None
        except:
            print("Record creation error: invalid url or user id")
            return None

        self.user = User.query.get(user)
        if not self.user:
            print("Record creation error: cannot query user from db")
            return None

        page = Page(url)
        if not page:
            print("Record creation error: page is None")
            return None
        self.domain_name = page.domain
        self.url = page.url
        self.page = page

    def write_page(self):

        self.domain = add_domain_to_db(self.domain_name, self.user)
        if not self.domain:
            print("Record write error: cannot add domain")
            return None
        if DEBUG_STATUS:
            print("Record write: domain added")

        self.link = add_link_to_db(self.url, self.domain, just_add=False)
        if not self.link:
            print("Record write error: cannot add current link")
            return None
        if DEBUG_STATUS:
            print("Record write: current link added")

        self.page.update(self.url)
        if not self.page.text:
            print("Record write error: page.text is None")
            return None
        self.text = add_text_to_db(self.page.text, self.link, self.domain)
        if not self.text:
            print("Record write error: cannot add text")
            return None
        if DEBUG_STATUS:
            print("Record write: text added")

        for url_link in self.page.links:
            url_link_added = add_link_to_db(url_link, self.domain, just_add=True)
            if not url_link_added:
                print("Record write error: cannot add link from page.links")

        if DEBUG_STATUS:
            print("Record write: links added")

        self.next_url = Link.query.filter(
                                          Link.domain == self.domain,
                                          Link.visits_count < self.link.visits_count
                                         ).first()
        if not self.next_url:
            print("Record write warning: not self.next_url")
            return True
        self.next_url = self.next_url.link
        if DEBUG_STATUS:
            print("Record write: next_url queried")

        return True