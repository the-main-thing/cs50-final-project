from flask_sqlalchemy import SQLAlchemy
from flask_login import UserMixin
db = SQLAlchemy()

users_domains = db.Table("users_domains",
    db.Column("user", db.Integer, db.ForeignKey("user.id"), nullable=False),
    db.Column("site", db.Integer, db.ForeignKey("domain.id"), nullable=False)
    )

class User(UserMixin, db.Model):
    id = db.Column(db.Integer, primary_key=True)
    email = db.Column(db.String(200), unique=True, nullable=False)
    password = db.Column(db.Text, nullable=False)
    invitation_key_id = db.Column(db.Integer, db.ForeignKey("invitation_key.id"), nullable=False)
    sites = db.relationship("Domain", secondary=users_domains, lazy="subquery", backref=db.backref("users", lazy=True))

    def __init__(self, email=None, password=None, invitation_key=None):
        self.email = email
        self.password = password
        self.invitation_key = invitation_key

    def __repr__(self):
        return '<email %r>' % self.email

class Domain(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    domain = db.Column(db.String(200), unique=True, nullable=False)
    links = db.relationship("Link", lazy="dynamic", backref=db.backref("domain", lazy="joined"))
    text = db.relationship("Text", lazy="dynamic", backref=db.backref("domain", lazy="joined"))

    def __init__(self, domain=None):
        self.domain = domain

    def __repr__(self):
        return "<Domain:{}>".format(self.domain)

class Link(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    link = db.Column(db.String(2000), unique=True, nullable=False)
    visits_count = db.Column(db.Integer, default=0, nullable=False)
    domain_id = db.Column(db.Integer, db.ForeignKey("domain.id"), nullable=False)
    text = db.relationship("Text", backref="link", lazy="dynamic")

    def __init__(self, link=None, domain=None, visits_count=0):
        self.link = link
        self.visits_count = visits_count
        self.domain = domain

    def __repr__(self):
        return "<Link:{}>".format(self.link)

class Text(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    # text is unique for cases where link leads to generated form like thread creation on some forum
    text = db.Column(db.Text, unique=True, nullable=False)
    link_id = db.Column(db.Integer, db.ForeignKey("link.id"), unique=True, nullable=False)
    domain_id = db.Column(db.Integer, db.ForeignKey("domain.id"), nullable=False)

    def __init__(self, text=None, link=None, domain=None):
        self.text = text
        self.link = link
        self.domain = domain

class Invitation_key(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    key = db.Column(db.String(7), unique=True, nullable=False)
    valid = db.Column(db.Boolean, default=False, nullable=False)
    can_load_site = db.Column(db.Boolean, default=False, nullable=False)
    users = db.relationship("User", lazy="dynamic", backref=db.backref("invitation_key", lazy="joined"))
