var sites = document.getElementsByClassName('sites');
for (let site of sites) {
    site.addEventListener('click', function(){
        // set 'src' attribute for iframe in generated code
        let applicationUrl = document.getElementById('getApplicationUrl').innerText;
        let codeBlockApplicationUrl = document.getElementById('applicationUrl');
        codeBlockApplicationUrl.innerText = applicationUrl;
        // set domain name for parameters which will be used in 'src' attribute of iframe in generated code
        let domainName = site.innerText;
        let codeBlockSiteValue = document.getElementById('site');
        codeBlockSiteValue.innerText = domainName;
        document.getElementById('generated-code-container').className = '';
    });
}

// set vertical position of search input
var yPositions = document.getElementsByClassName('vertical-position');
for (let yPos of yPositions) {
    yPos.addEventListener('click', function() {
        let codeBlockYpos = document.getElementById('search-input-vertical-position');
        codeBlockYpos.innerText = yPos.innerText;
    });
}

// set vertical position of search input
var xPositions = document.getElementsByClassName('horisontal-position');
for (let xPos of xPositions) {
    xPos.addEventListener('click', function() {
        let codeBlockXpos = document.getElementById('search-input-horisontal-position');
        codeBlockXpos.innerText = xPos.innerText;
    });
}
