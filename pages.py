import re
from bs4 import BeautifulSoup, Comment
from url_normalize import url_normalize
from helpers import domain, render, resolve_link
from settings import RESTRICTED_TAGS, RESTRICTED_CLASSES


class Page():
    """
    Page will have several properties:
    1) Source from what page was get "self.source"
    2) Domain name of the site from wich page was get "self.domain"
    3) HTML code without tags like <script>, <style> etc. "self.html"
    4) Text extracted from html code "self.text"
    5) Set of links extracted from html code "self.links"
    """
    def __init__(self, source):
        """ Instantiate object """
        try:
            source = str(source)
            source = url_normalize(source)
        except:
            print("pages.py error: invalid source")
            return None
        self.url = source
        self.domain = domain(source)
        if not self.domain:
            print("pages.py error: can't extract domain from source")
            return None

    def update(self, source):
        """ Update the page. Render and parse html from given source """
        source = url_normalize(source)
        if not source:
            return None

        self.url = source
        self.domain = domain(source)
        self.html = BeautifulSoup(render(source, skip_url_check=True), "html.parser")
        self.links = set()

        # decompose tags that we don't need
        for element in self.html.find_all(RESTRICTED_TAGS):
            element.decompose()

        # decompose classes that we don't need
        for element in self.html.find_all(attrs={'class': re.compile(RESTRICTED_CLASSES),}):
            element.decompose()

        # decompose comments
        for comment in self.html.find_all(string=lambda text:isinstance(text,Comment)):
            comment.replace_with("")
        '''
        # decompose restricted tags again because it does not fully work on fotobiblioteka.ru
        for element in self.html.find_all(RESTRICTED_TAGS):
            element.decompose()
        '''

        # fill up links set
        for link in self.html.find_all("a", href=True):
            # turn html link into url address
            link = resolve_link(self.url, link["href"])
            domain_name = domain(link, True)
            # add only links from same domain name as current
            if domain_name == self.domain:
                self.links.add(link)

        # get text from html
        self.text = self.html.get_text()
        # remove empty lines
        self.text = self.text.split('\n')
        lines = []
        for line in self.text:
            line = line.strip()
            # line length have to be bigger than one character long
            if len(line) > 1:
                lines.append(line)
        self.text = "\n".join(lines)