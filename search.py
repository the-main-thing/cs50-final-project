from model import Text, Link, Domain
from helpers import domain
from settings import DEBUG_STATUS, SEARCH_LEFT_SIDE_CHARACTERS, SEARCH_RIGHT_SIDE_CHARACTERS

def search(query, site):
    try:
        query = str(query)
        site = domain(str(site))
    except:
        return None
    site = Domain.query.filter_by(
                                  domain=site
                                  ).first()
    if not site:
        if DEBUG_STATUS:
            print("search.py: site is None")
        return None

    # if long query remove few characters from begigng of the word. It helps with just one word
    # original_query = query
    query = query.lower()
    query_length = len(query)
    left_padding = 0
    right_padding = 0
    if len(query) > 7:
        left_padding = 1
        right_padding = 2
        query = query[left_padding:-right_padding]

    matches = Text.query.filter(
                                Text.domain == site,
                                Text.text.ilike("%{0}%".format(query))
                                ).all()
    results = []
    for match in matches:
        if query in match.text.lower():
            text = match.text
            try:
                query_index = text.lower().index(query)
                left_text = text[:(query_index - left_padding)]
                right_text = text[(query_index + query_length + right_padding):]
                left_text = left_text[SEARCH_LEFT_SIDE_CHARACTERS:]
                right_text = right_text[:SEARCH_RIGHT_SIDE_CHARACTERS]
                query_to_display = text[(query_index - left_padding):(query_index + query_length + right_padding)]
                #left_text = text[0][PADDING_LEFT:]
                #right_text = text[1][:PADDING_RIGHT]
                #text = """...{0}<b>{1}</b>{2}...""".format(text[0][PADDING_LEFT:], query, text[1][:PADDING_RIGHT])
                results.append(
                                {
                                    "link" : match.link.link,
                                    "left_text": left_text,
                                    "query": query_to_display,
                                    "right_text": right_text,
                                    "domain": site.domain
                                })
            except IndexError:
                pass
    return results
