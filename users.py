from sqlalchemy.exc import IntegrityError
from werkzeug.security import generate_password_hash, check_password_hash
from flask_login import LoginManager, UserMixin, login_user, login_required

from model import db, User, Invitation_key

login_manager = LoginManager()
@login_manager.user_loader
def load_user(user_id):
    return User.query.get(int(user_id))

def register(email=None, password=None, invitation_key=None):
    """ Adding user to a database. Returns None if succeed """

    if not email or not password or not invitation_key:
        return "parameters error"
    invitation_key = Invitation_key.query.filter_by(key=invitation_key).first()
    if not invitation_key.valid:
        return "invitation error"
    password = generate_password_hash(password)
    print("trying to add user to db")
    try:
        user = User(email, password, invitation_key)
        db.session.add(user)
        db.session.commit()
        print("user added")
    except IntegrityError:
        db.session.rollback()
        print("user not added")
        return "user creation error"
    print("register will now return None")
    return None

def log_in(email=None, password=None, remember=False):
    """ Logging user in. Return user db row if everything is ok """
    if not email or not password:
        return "form error"
    user = User.query.filter_by(email=email).first()
    if not user:
        return "no such user"
    if not check_password_hash(user.password, password):
        return "wrong password"
    login_user(user, remember=remember)
    return None

def add_invitation_keys(keys):
    for key in keys:
        try:
            invite = Invitation_key(key=key["key"], valid=key["valid"], can_load_site=key["can_load_site"])
            db.session.add(invite)
            db.session.commit()
        except:
            db.session.rollback()
            pass
    return None