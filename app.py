import os
import threading
'''
from flask import Flask, request, render_template, redirect, url_for, jsonify
from flask_login import logout_user, current_user, login_required
from flask_bootstrap import Bootstrap

from search import search
from users import login_manager, register, log_in, add_invitation_keys
from model import db, Text
from site_loader import load_site
from forms import SignupForm, LoginForm, LoadForm
from settings import DB_PATH, DEBUG_STATUS, APPLICATION_SECRET_KEY, INVITATION_KEYS, APPLICATION_ABSOLUTE_URL
'''
from flask import Flask, request, render_template
app = Flask(__name__)
app.secret_key = 123#APPLICATION_SECRET_KEY
'''app.config["SQLALCHEMY_DATABASE_URI"] = DB_PATH
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False
db.init_app(app)
db.app = app
db.create_all()
db.session.commit()
login_manager.init_app(app)
login_manager.login_view = "login"
Bootstrap(app)
'''
# ensure responses aren't cached (from cs50/pset7 by Harvard's cs50 staff)
'''if app.config["DEBUG"]:
    @app.after_request
    def after_request(response):
        response.headers["Cache-Control"] = "no-cache, no-store, must-revalidate"
        response.headers["Expires"] = 0
        response.headers["Pragma"] = "no-cache"
        return response

# add invitation keys to db
add_invitation_keys(INVITATION_KEYS)

@app.route("/")
def index():
    return redirect(url_for("dashboard"))

@app.route("/dashboard")
@login_required
def dashboard():
    sites = tuple(str(site.domain) for site in (current_user.sites))
    #return jsonify(sites)
    return render_template("dashboard.html", sites=sites, absolute_path_to_app=APPLICATION_ABSOLUTE_URL)

@app.route("/load", methods=["GET", "POST"])
@login_required
def load():
    form = LoadForm()
    if request.method == "GET":
        return render_template("load.html", form=form)
    if not form.validate_on_submit():
        return redirect(url_for("load"))
    try:
        source = str(form.url.data)
        depth = int(form.depth.data)
    except:
        source, depth = None, None
    loading = threading.Thread(target=load_site, args=(source, current_user.id, depth))#load_site(source, depth)
    loading.daemon = True
    loading.start()
    return redirect(url_for("dashboard"))

@app.route("/search")
def do_search():
    query = request.args["q"]
    try:
        site = request.args["amp;s"]
    except:
        site = request.args["s"]
    result = search(query, site)
    return render_template("search.html", results=result)

@app.route("/signup", methods=["GET", "POST"])
def signup():
    form = SignupForm()
    if request.method == "GET":
        return render_template("signup.html", form=form)
    # validate form data
    if not form.validate_on_submit():
        return redirect(url_for("signup"))
    # add user to database
    email = str(form.email.data)
    password = str(form.password.data)
    invitation_key = str(form.invitation_key.data)
    err = register(email, password, invitation_key)
    # if err is not None, something went wrong
    if err:
        errors = {
            "parameters error" : "Invalid form",
            "invitation error" : "Invalid invitation key",
            "user error" : "User already exist",
            "password error" : "Password is to weak"
        }
        if DEBUG_STATUS:
            return errors[err]
        return redirect(url_for("signup"))
    return redirect(url_for("login"))

@app.route("/login", methods=["GET", "POST"])
def login():
    form = LoginForm()
    if request.method == "GET":
        return render_template("login.html", form=form)
    if not form.validate_on_submit():
        return redirect(url_for("login"))
    email = form.email.data
    password = form.password.data
    err = log_in(email, password, form.remember.data)
    if err:
        errors = {
                 "form error" : "No email or password provided",
                 "no such user" : "No such user",
                 "wrong password": "Wrong password"
                 }
        if DEBUG_STATUS:
            return errors[err]
        return redirect(url_for("login"))
    return redirect(url_for("dashboard"))

@app.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('index'))
'''
@app.route("/block")
def block():
    return render_template('block.html')

if __name__ == "__main__":
    #app.run(debug=DEBUG_STATUS, host=os.getenv('IP', '0.0.0.0'),port=int(os.getenv('PORT', 8080)))
    app.run(debug=True, host=os.getenv('IP', '0.0.0.0'),port=int(os.getenv('PORT', 8080)))