from flask_wtf import FlaskForm
from wtforms import StringField, PasswordField, BooleanField, IntegerField
from wtforms.validators import InputRequired, NumberRange, Email, Length, ValidationError

def invitation_length_check(form, field):
    """ Function used in wtforms validator in Signup form to check length of invitation key """
    if len(field.data) != 7:
        raise ValidationError("Invalid invitation key")

class SignupForm(FlaskForm):
    email = StringField("Email", validators=[
                                            InputRequired(message="Email required"),
                                            Email(message='Invalid email'),
                                            Length(min=5, max=200)
                                            ])
    password = PasswordField("Password", validators=[
                                                    InputRequired(message="Password required"),
                                                    Length(min=8, max=80)
                                                    ])
    invitation_key = PasswordField("Invitation key", validators=[
                                                                InputRequired(message="Invitation key required"),
                                                                invitation_length_check
                                                                ])

class LoginForm(FlaskForm):
    email = StringField("Email", validators=[
                                            InputRequired(message="Email required"), Email(message='Invalid email'),
                                            Length(min=5, max=200)
                                            ])
    password = PasswordField("Password", validators=[
                                                    InputRequired(message="Password required"),
                                                    Length(min=8, max=80)
                                                    ])
    remember = BooleanField('remember me')

class LoadForm(FlaskForm):
    url = StringField("Url", validators=[
                                        InputRequired(message="Must provide url"),
                                        Length(min=2, max=2000)
                                        ])
    depth = IntegerField("How many links to follow (Optional)", validators=[
                                                                            NumberRange(1, 99, "Must provide number between 0 and 100")
                                                                            ], default="50")