# URL for app to use it in client side script. Add without ending slash '/'
APPLICATION_ABSOLUTE_URL = "https://ide50-the-main-thing.cs50.io:8080"
APP_LOCAL_PATH = r"/home/ubuntu/workspace/final_project/search/"
# In order to use crawler you need to download chrome driver
WEBDRIVER_PATH = APP_LOCAL_PATH + "chromedriver"
DB_PATH = "sqlite:///" + APP_LOCAL_PATH + "Pages.db"
RESTRICTED_TAGS = ["script", "noscript", "style", "link", "meta", "input", "php", "nav", "button", "menu"]
RESTRICTED_CLASSES = "nav|footer|login|logout|signup"
DEBUG_STATUS = True
APPLICATION_SECRET_KEY = "&hS8$@$%reFD8}[xc76jzoi(Ydch{do9%g4xc^45#@GReg!f5QS{df24|\]PJK"
# Invitation keys for user registration
INVITATION_KEYS = [
                    {
                        "key": "hello50",
                        "valid": True,
                        "can_load_site": True
                    },
                    {
                        "key": "example",
                        "valid": False,
                        "can_load_site": False
                    }
                  ]
# amount of characters to be displayed if search results on left and right side of query words
# minus is for left side padding -----5-4-3-2-1-0-1-2-3-4-5----->
SEARCH_LEFT_SIDE_CHARACTERS = -70
SEARCH_RIGHT_SIDE_CHARACTERS = 150